/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"flag"
	"log"
	"os"
	"os/user"
	"strings"

	"github.com/nlopes/slack"
	"gitlab.com/asadana/mr-ro-bot/internal"
	"gopkg.in/natefinch/lumberjack.v2"
)

func main() {
	isModeDebug := readCommandLineFlags()
	botDir := initDirStructure()
	// The token should come from env variable on the system bot runs on.
	token := os.Getenv(internal.EnvSlackMrRoBotOAuthToken)
	veriToken := os.Getenv(internal.EnvSlackMrRoBotVerficationToken)
	log.SetOutput(&lumberjack.Logger{
		Filename:   botDir.BotLogDir + "/" + "mrrobot.log",
		MaxSize:    10, // megabytes
		MaxBackups: 4,
		MaxAge:     28,   //days
		Compress:   true, // disabled by default
	})
	if token == "" {
		log.Fatalf("[ERROR] Slack OAuth token not set up.")
	} else {
		api := slack.New(token)
		if isModeDebug {
			log.Println("[DEBUG] Debug mode is enabled")
		}
		if veriToken == "" {
			log.Fatalf("[ERROR] Slack Verification token not set up. Could not start slash server.")
		} else {
			bot := internal.NewBot(api.NewRTM(), botDir, isModeDebug)
			bot.Start(api, veriToken)
		}
	}
}

func readCommandLineFlags() (isModeDebug bool) {
	runModePtr := flag.String(internal.MrRoBotFlagModeName, internal.MrRoBotFlagModeDefault, internal.MrRoBotFlagModeDescription)
	flag.Parse()
	runModeString := strings.TrimRight(*runModePtr, "\n")
	if strings.EqualFold(internal.MrRoBotFlagModeOptionDebug, runModeString) {
		isModeDebug = true
	} else {
		isModeDebug = false
	}
	return isModeDebug
}

func initDirStructure() (botDir internal.BotDir) {
	usr, err := user.Current()
	if err != nil {
		log.Fatalln(err)
	}

	botDir = internal.BotDir{
		BotUserDir:    strings.Join([]string{usr.HomeDir, internal.MrRoBotUserDir}, "/"),
		BotStandupDir: strings.Join([]string{usr.HomeDir, internal.MrRoBotUserDir, internal.StandupReportDir}, "/"),
		BotLogDir:     strings.Join([]string{usr.HomeDir, internal.MrRoBotUserDir, internal.MrRoBotLogDir}, "/"),
	}

	if !internal.FileExists(botDir.BotUserDir) {
		os.Mkdir(botDir.BotUserDir, os.ModePerm)
	}
	if !internal.FileExists(botDir.BotStandupDir) {
		os.Mkdir(botDir.BotStandupDir, os.ModePerm)
	}
	if !internal.FileExists(botDir.BotLogDir) {
		os.Mkdir(botDir.BotLogDir, os.ModePerm)
	}
	return botDir
}
