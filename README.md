# Mr. Ro Bot

[![pipeline status](https://gitlab.com/asadana/mr-ro-bot/badges/slash-standup/pipeline.svg)](https://gitlab.com/asadana/mr-ro-bot/commits/slash-standup)
[![coverage report](https://gitlab.com/asadana/mr-ro-bot/badges/slash-standup/coverage.svg)](https://gitlab.com/asadana/mr-ro-bot/commits/slash-standup)
---

Open Source Slack bot written in GoLang

---
### Features

 - Accepts bot admin users through env
 - Standup functionality
    - Configurable questions to ask
    - Record responses
    - Ask multiple people
    - Skip Ignored Users
    - Generate a JSON report after everyone has completed

---
### How to Build and Deploy

TODO

Environment Variables expected:

   > SLACK_MR_RO_BOT_TOKEN    <- OAuth Bot User token for accessing your workspace after Bot installation.

   > SLACK_MR_RO_BOT_ADMIN_USERS <- Comma seperated user real names that will be marked as bot admins. Atleast one valid required.

---
### LICENSE
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or  any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.