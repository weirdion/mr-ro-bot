/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package internal to handle inner-workings of the bot
package internal

import (
	"log"
	"os"
	"strings"
	"time"

	"github.com/nlopes/slack"
)

// MrRoHandler struct to provide a place to access slackBotAdmins, slackUsers and StandupHandler
type MrRoHandler struct {
	SlackBotAdmins []slack.User
	SlackUsers     []slack.User
	StandupHandler *StandupHandler
}

// NewHandler function to create a new var of MrRoHandler
func NewHandler(isModeDebug bool) *MrRoHandler {
	h := &MrRoHandler{
		SlackUsers:     nil,
		SlackBotAdmins: nil,
		StandupHandler: &StandupHandler{
			TimeStamp:        time.Now(),
			IsStandupRunning: false,
			StandupResponses: nil,
			isModeDebug:      isModeDebug,
		},
	}
	log.Println("[INFO] Handler created")
	return h
}

// ReadSlackBotAdmins function to read slack bot admins from the env.
// If no valid admins are found, the RTM is disconnected.
func (handler *MrRoHandler) ReadSlackBotAdmins(rtm *slack.RTM) {
	log.Println("[INFO] Detecting admins...")
	envSlackBotAdminVar := os.Getenv(EnvSlackMrRoBotAdminUsers)
	var envSlackBotAdmins []string
	if envSlackBotAdminVar != "" {
		beforeTrimSlice := strings.Split(envSlackBotAdminVar, ",")
		for _, element := range beforeTrimSlice {
			envSlackBotAdmins = append(envSlackBotAdmins, strings.TrimSpace(element))
		}
	} else {
		log.Fatalf("[ERROR] Environment variable for SlackBot admin not set.")
	}

	handler.SlackUsers = handler.SlackUsers[:0] // Clear existing slack users
	slackUsers, err := rtm.Client.GetUsers()
	if err != nil {
		log.Fatalf("[ERROR] Failed to retrieve Users: %v", err)
	} else {
		handler.SlackUsers = slackUsers
		if handler.StandupHandler.isModeDebug {
			log.Printf("[DEBUG] Finished detecting %v users for Team %v", len(slackUsers), slackUsers[0].Profile.Team)
		}
	}

	handler.SlackBotAdmins = handler.SlackBotAdmins[:0] // Clear existing slack bot admins
	var slackBotAdminSlice []slack.User
	for _, user := range slackUsers {
		if !user.IsBot {
			for _, envUser := range envSlackBotAdmins {
				if envUser == user.RealName {
					log.Println("[INFO] Admin user found and added: " + user.ID)
					slackBotAdminSlice = append(slackBotAdminSlice, user)
				}
			}
		}
	}
	if slackBotAdminSlice == nil {
		log.Fatalf("[ERROR] There no valid admin users detected.")
	} else {
		handler.SlackBotAdmins = slackBotAdminSlice
	}
}

// IsSlackBotAdmin is a function that takes id and returns true if they are an admin, false if not
func (handler *MrRoHandler) IsSlackBotAdmin(userID string) bool {
	for _, element := range handler.SlackBotAdmins {
		if element.ID == userID {
			return true
		}
	}
	return false
}
