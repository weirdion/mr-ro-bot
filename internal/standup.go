/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
// Package internal to handle inner-workings of the bot
package internal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"strings"
	"time"

	"github.com/nlopes/slack"
)

// StandupConfig struct is a json format for creating/reading json files for standup configuration
type StandupConfig struct {
	Channel      string   `json:"channel_membership"` // Channel name to use to check membership for running standup
	IgnoredUsers []string `json:"ignored_users"`      // Users to skip during standup
	Questions    []string `json:"questions"`          // Questions to be asked during standup
}

// StandupReport struct is a json formatted structure to generate reports
type StandupReport struct {
	TimeStamp    string             `json:"timestamp"`
	IgnoredUsers []string           `json:"ignored_users"`
	Reports      []standupSubReport `json:"reports"`
}

type standupSubReport struct {
	Name        string            `json:"user_name"`
	Skipped     bool              `json:"standup_skipped"`
	ResponseMap map[string]string `json:"responses"`
}

// StandupHandler is a struct to handle running standups
type StandupHandler struct {
	IsStandupRunning bool
	TimeStamp        time.Time
	ChannelID        string // Channel ID for the membership criteria
	StandupStartedBy string
	ReportName       string
	IgnoredUsers     []standupIgnoredUser
	StandupResponses []standup
	isModeDebug      bool
}

// StandupResponse is a struct to store and parse response by each user
type standup struct {
	Name      string
	UserID    string
	ChannelID string
	Started   bool
	Finished  bool
	Responses response
	Skipped   bool
}

type standupIgnoredUser struct {
	ID   string
	Name string
}

type response struct {
	ResponseMap map[string]string
	Keys        []string
}

// StandupCheckReport struct provides a quick check on who has finished the standup and who is left
type StandupCheckReport struct {
	Name        string
	HasFinished bool
}

// PopulateIgnoredUsers func inputs the StandupConfig and slice of slackUser to populate the StandupHandler ignored users
func (standupHandler *StandupHandler) PopulateIgnoredUsers(slackUsers []slack.User, config StandupConfig) {
	standupHandler.IgnoredUsers = standupHandler.IgnoredUsers[:0]
	for _, user := range slackUsers {
		for _, ignoredName := range config.IgnoredUsers {
			if user.RealName == ignoredName {
				standupHandler.IgnoredUsers = append(standupHandler.IgnoredUsers, standupIgnoredUser{ID: user.ID, Name: user.RealName})
			}
		}
	}
	if standupHandler.isModeDebug {
		log.Printf("[DEBUG] Ignored users: %v", standupHandler.IgnoredUsers)
	}
}

// StartStandup function to start the standup
func (standupHandler *StandupHandler) StartStandup(rtm *slack.RTM, config StandupConfig, startedByAdmin string) (isStandupStarted bool, standupStartFailedReason string) {

	if standupHandler.isModeDebug {
		log.Printf("[DEBUG] Starting standup with the config: %v", config)
	}
	var channelScope []string
	channelScope = append(channelScope, "private_channel")
	channelScope = append(channelScope, "public_channel")
	channels, _, err1 := rtm.Client.GetConversations(&slack.GetConversationsParameters{
		Types: channelScope})
	var channel slack.Channel
	channelFound := false
	if err1 != nil {
		log.Printf("[ERROR] Failed to get Channel List info %s", err1)
		isStandupStarted = false
		standupStartFailedReason = StandupStartFailChannelList
	} else {
		for _, convo := range channels {
			if convo.Name == config.Channel {
				channel = convo
				channelFound = true
				break
			}
		}
		if !channelFound {
			log.Printf("[ERROR] Failed to find Channel info for: %s", config.Channel)
			isStandupStarted = false
			standupStartFailedReason = StandupStartFailChannel
		} else {
			members, _, err2 := rtm.Client.GetUsersInConversation(&slack.GetUsersInConversationParameters{ChannelID: channel.ID})
			if err2 != nil {
				log.Printf("[ERROR] Failed to get List of users in Conversation: %s", err2)
				isStandupStarted = false
				standupStartFailedReason = StandupStartFailUsersInChannel
			} else {
				standupHandler.IsStandupRunning = true
				standupHandler.TimeStamp = time.Now().Local() // Reset the timestamp
				standupHandler.ChannelID = channel.ID
				standupHandler.StandupStartedBy = startedByAdmin
				standupHandler.StandupResponses = standupHandler.StandupResponses[:0] // Clear previous responses
				standupHandler.ReportName = ""                                        // Clear previous report name
				isStandupStarted = true
				standupStartFailedReason = ""
				log.Printf("[INFO] Standup started successfully on channel %s.", channel.Name)
				for _, member := range members {
					memberInfo, _ := rtm.Client.GetUserInfo(member)
					if !memberInfo.IsBot && !memberInfo.Deleted {
						isIgnored := false
						for _, ignoredUser := range standupHandler.IgnoredUsers {
							if ignoredUser.ID == memberInfo.ID {
								isIgnored = true
								break
							}
						}
						if !isIgnored {
							_, _, memberChannelID, err3 := rtm.Client.OpenIMChannel(memberInfo.ID)
							if err3 != nil {
								log.Printf("[ERROR] Failed to start a conversation with %s.\n%s", memberInfo.RealName, err3)
							} else {
								if standupHandler.isModeDebug {
									log.Printf("[DEBUG] Talking to %s memberChannelID %s", memberInfo.RealName, memberChannelID)
								}
								defaultResponse := response{
									ResponseMap: make(map[string]string, len(config.Questions)),
									Keys:        []string{}}
								for _, question := range config.Questions {
									defaultResponse.ResponseMap[question] = ""
									defaultResponse.Keys = append(defaultResponse.Keys, question)
								}
								standupUser := standup{
									Name:      memberInfo.RealName,
									UserID:    memberInfo.ID,
									ChannelID: memberChannelID,
									Started:   false,
									Finished:  false,
									Responses: defaultResponse,
									Skipped:   false}
								standupHandler.StandupResponses = append(standupHandler.StandupResponses, standupUser)
								SendRegularMessage(rtm, StandupStartMessage, memberChannelID)
							}
						} else {
							log.Printf("[INFO] Skipping user: %s", memberInfo.ID)
						}
					}
				}
			}
		}
	}
	return isStandupStarted, standupStartFailedReason
}

//
func (standupHandler *StandupHandler) AskNextQuestion(rtm *slack.RTM, messageText string, channelID string) (isUserDone bool) {
	for index, standupElement := range standupHandler.StandupResponses {
		if standupElement.ChannelID == channelID {
			if !standupElement.Finished {
				keys := standupElement.Responses.Keys
				if !standupElement.Started && !standupElement.Skipped {
					commandCheck := strings.TrimSpace(strings.ToLower(messageText))
					switch commandCheck {
					case "start":
						standupHandler.StandupResponses[index].Started = true
						SendRegularMessage(rtm, standupElement.Responses.Keys[0], channelID)
						standupHandler.ExportReport()

					case "skip":
						standupHandler.StandupResponses[index].Skipped = true
						standupHandler.StandupResponses[index].Finished = true
						SendRegularMessage(rtm, StandupUserSkip, channelID)
						standupHandler.ExportReport()
						return true
					}
				} else {
					// Check if any questions are unanswered
					isQuestionLeft := true
					for keyIndex := 0; keyIndex < len(keys); keyIndex++ {
						if standupHandler.StandupResponses[index].Responses.ResponseMap[keys[keyIndex]] == "" {
							standupHandler.StandupResponses[index].Responses.ResponseMap[keys[keyIndex]] = messageText
							if keyIndex+1 == len(keys) {
								isQuestionLeft = false
							} else {
								SendRegularMessage(rtm, standupElement.Responses.Keys[keyIndex+1], channelID)
								break
							}
						}
					}
					if !isQuestionLeft {
						log.Printf("[INFO] Standup completed for user: %s", standupElement.UserID)
						standupHandler.StandupResponses[index].Finished = true
						SendRegularMessage(rtm, StandupUserFinished, channelID)
						standupHandler.ExportReport()
						return true
					}
					standupHandler.ExportReport()
				}
			} else {
				SendRegularMessage(rtm, StandupUserAlreadyFinished, channelID)
			}
			break
		}
	}
	return false
}

//
func (standupHandler *StandupHandler) CheckIfStandupIsComplete() (isStandupComplete bool, usersStandupReport []StandupCheckReport) {
	isStandupComplete = true
	for _, standupInstance := range standupHandler.StandupResponses {
		usersStandupReport = append(usersStandupReport, StandupCheckReport{
			Name:        standupInstance.Name,
			HasFinished: standupInstance.Finished})
		if !standupInstance.Finished {
			isStandupComplete = false
		}
	}
	return isStandupComplete, usersStandupReport
}

//
func (standupHandler *StandupHandler) PrintReport(rtm *slack.RTM, channelID string) {
	SendRegularMessage(rtm, fmt.Sprintf("%v", generateReport(standupHandler)), channelID)
}

// ExportReport func uses generateReport func to create the report, then writes it to the file
func (standupHandler *StandupHandler) ExportReport() (errs error) {
	usr, err := user.Current()
	if err != nil {
		log.Printf("[ERROR] Failed to detect current user trying to export report: %v", err)
		return err
	}

	reportDir := strings.Join([]string{usr.HomeDir, MrRoBotUserDir, StandupReportDir}, "/")
	if !FileExists(reportDir) {
		os.Mkdir(reportDir, os.ModePerm)
	}

	if FileExists(reportDir) {
		var reportGenerationError error
		// While standup is running, create/replace tmp file with all content
		if standupHandler.IsStandupRunning {
			standupReport := generateReport(standupHandler)
			// Make it look pretty
			byteSlice, _ := json.MarshalIndent(standupReport, "", "\t")
			// If reportName is empty, generate a new file name
			if standupHandler.ReportName == "" {
				fileName := strings.Join([]string{reportDir, StandupReportPrefix}, "/")
				fileName = strings.Join([]string{fileName, standupReport.TimeStamp, ".json"}, "")
				standupHandler.ReportName = fileName
			}
			fileName := standupHandler.ReportName
			fileName = strings.Join([]string{fileName, ".tmp"}, "")
			if standupHandler.isModeDebug {
				log.Printf("[DEBUG] Writing temporary report to %s", fileName)
			}
			reportGenerationError = ioutil.WriteFile(fileName, byteSlice, 0644)
		} else {
			// When standup is completed, rename the tmp file
			// This will be replaced by taking the json and generating PDF
			fileName := standupHandler.ReportName
			tmpFileName := strings.Join([]string{fileName, ".tmp"}, "")
			if FileExists(tmpFileName) {
				reportGenerationError = os.Rename(tmpFileName, fileName)
				log.Printf("[INFO] Writing report to %s", fileName)
			}
		}

		return reportGenerationError

	}
	log.Println("[ERROR] Unable to access the report directory.")
	return fmt.Errorf("[ERROR] Failed to access the directory %s", reportDir)
}

func generateReport(standupHandler *StandupHandler) (standupReport StandupReport) {
	standupReport = StandupReport{TimeStamp: standupHandler.TimeStamp.Format(StandupTimeStampFormat)}
	for _, ignoredUser := range standupHandler.IgnoredUsers {
		standupReport.IgnoredUsers = append(standupReport.IgnoredUsers, ignoredUser.Name)
	}
	for _, standupElement := range standupHandler.StandupResponses {
		subReport := standupSubReport{
			Name:        standupElement.Name,
			Skipped:     standupElement.Skipped,
			ResponseMap: standupElement.Responses.ResponseMap}
		standupReport.Reports = append(standupReport.Reports, subReport)
	}
	return standupReport
}

func (standupHandler *StandupHandler) findStandupByUser(userID string) (standupInstance standup) {
	for _, standupElement := range standupHandler.StandupResponses {
		if standupElement.UserID == userID {
			return standupElement
		}
	}
	return standup{}
}

func parseStandupCheckReport(standupCheckReport []StandupCheckReport) (checkReportString string) {
	var stringBuilder strings.Builder
	stringBuilder.WriteString("Standup Results [Name: IsFinished] :")
	for _, report := range standupCheckReport {
		stringBuilder.WriteString(fmt.Sprintf("\n%s: %v", report.Name, report.HasFinished))
	}
	return stringBuilder.String()
}
