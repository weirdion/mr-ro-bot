/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package internal to handle inner-workings of the bot
package internal

import (
	"fmt"
	"log"
	"strings"

	"github.com/nlopes/slack"
)

// OnMessageReceived is an exposed method to handle different kinds of slack messages received.
func (bot *Bot) OnMessageReceived(message *slack.MessageEvent) {
	info := bot.RTM.GetInfo()
	prefix := fmt.Sprintf("<@%s> ", info.User.ID)

	if message.User != info.User.ID { // Make sure we are not self-talking
		if strings.HasPrefix(message.Text, prefix) { // Check if bot had @ put in front
			if bot.Handler.IsSlackBotAdmin(message.User) {
				log.Printf("[INFO] Admin command received from %s", message.User)
				DoAdminsBidding(bot, message.Msg, prefix)
			} else {
				SendRegularMessage(bot.RTM, "Sorry, only admins can tell me what to do.", message.Channel)
			}
		} else if bot.Handler.StandupHandler.IsStandupRunning {
			standupInstance := bot.Handler.StandupHandler.findStandupByUser(message.User)
			if bot.isModeDebug {
				log.Printf("[DEBUG] StandupInstance find by user: %v", standupInstance)
			}
			if standupInstance.ChannelID == message.Channel {
				if bot.Handler.StandupHandler.AskNextQuestion(bot.RTM, message.Text, message.Channel) {
					// TODO: this should be removed when scheduling is implemented
					if isComplete, _ := bot.Handler.StandupHandler.CheckIfStandupIsComplete(); isComplete {
						log.Println("[INFO] Standup has completed.")
						bot.standupStop(bot.Handler.StandupHandler.StandupStartedBy)
					}
				}
			} else {
				log.Printf("[WARN] Unkown user messaging the bot during standup: %v", message.User)
			}
		} // else {
		// SendRegularMessage(rtm, "Sorry, the standup has already finished or not started yet.", message.Channel)
		//}
	}
}

// SendRegularMessage function provides a simple way to send text-only messages.
func SendRegularMessage(rtm *slack.RTM, messageString string, channelID string) {
	if rtm != nil && messageString != "" && channelID != "" {
		rtm.SendMessage(rtm.NewOutgoingMessage(messageString, channelID))
	} else {
		log.Println("[ERROR] sendRegularMessage: Failed to send message")
	}
}

//
func SendMessageToUser(rtm *slack.RTM, userID string, message string) {
	if rtm != nil && message != "" && userID != "" {
		userInfo, _ := rtm.Client.GetUserInfo(userID)
		_, _, memberChannelID, err := rtm.Client.OpenIMChannel(userInfo.ID)
		if err != nil {
			log.Printf("[ERROR] Failed to start a conversation with %s.\n%s", userInfo.RealName, err)
		} else {
			SendRegularMessage(rtm, message, memberChannelID)
		}
	} else {
		log.Println("[ERRPR] sendMessageToUser: Failed to send message.")
	}
}
