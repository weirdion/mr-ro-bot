/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package internal to handle inner-workings of the bot
package internal

const (
	// EnvSlackMrRoBotOAuthToken const is Env variable with the bot OAuth token
	EnvSlackMrRoBotOAuthToken = "SLACK_MR_RO_BOT_OAUTH_TOKEN"
	// EnvSlackMrRoBotVerficationToken const is Env variable with bot verification token
	EnvSlackMrRoBotVerficationToken = "SLACK_MR_RO_BOT_VERIFICATION_TOKEN"
	// EnvSlackMrRoBotAdminUsers const is the Env variable with the Admin users' (full name) to control the bot
	EnvSlackMrRoBotAdminUsers = "SLACK_MR_RO_BOT_ADMIN_USERS"
	// MrRoBotUserDir const is the main directory for the bot
	MrRoBotUserDir = ".mrrobot"
	// MrRoBotLogDir const is the sub-directory for the bot's logs
	MrRoBotLogDir = "logs"
	// MrRoBot flag RunMode name
	MrRoBotFlagModeName = "mode"
	// MrRoBot flag RunMode option debug
	MrRoBotFlagModeOptionDebug = "debug"
	// MrRoBot flag RunMode option prod
	MrRoBotFlagModeOptionProd = "prod"
	// MrRoBot flag RunMode default
	MrRoBotFlagModeDefault = "prod"
	// MrRoBot flag RunMode description
	MrRoBotFlagModeDescription = "Running mode value, default prod. Possible values \"prod\", \"debug\""
	// AcceptAdminCommandResponse const is the response for accepted commands from admins
	AcceptAdminCommandResponse = "Yes Master! Your wish will shall be done."
	// RejectAdminCommandResponse const is the response for commands from admins that are not registered
	RejectAdminCommandResponse = "Sorry Master, I don't understand what you have asked of me."
	// RejectNonAdminResponse const is the response to users that are not admins
	RejectNonAdminResponse = "Sorry, only my Master can command me."
	// StandupConfigPath const is the file name that the bot generates/reads for standup configuration
	StandupConfigPath = "config.json"
	// StandupReportDir const is the directory under MrRoBotUserDir that is used for storing reports
	StandupReportDir = "standup-reports"
	// StandupReportPrefix const is the prefix used for the standup reports generated
	StandupReportPrefix = "standup-report-"
	// StandupTimeStampFormat const is the format used for timestamp in reports
	StandupTimeStampFormat = "2006-01-02-03-04-pm"
	// StandupConfigFailed const is the response sent to admin if config.json cannot be read/exist
	StandupConfigFailed = "Sorry, I could not load the config to start the standup."
	// StandupStarted const is the response sent to the admin when starting standup from /standup start
	StandupStarted = "Yes Master, I shall start the Standup now."
	// StandupStartFailChannelList const is the response sent to the admin when standup failed
	StandupStartFailChannelList = "Master, I could not retrieve channel list for the standup to start."
	// StandupStartFailChannel const is the response sent to the admin when standup failed
	StandupStartFailChannel = "Master, I could not find the channel for the standup to start."
	// StandupStartFailUsersInChannel const is the response sent to the admin when standup failed
	StandupStartFailUsersInChannel = "Master, I do not access to get the list of users in the channel."
	// StandupAlreadyRunning const is the response sent to the admin when standup is already ongoing and /standup start is received
	StandupAlreadyRunning = "Sorry Master, the Standup is already ongoing."
	// StandupNotOngoing const is the response to the admin when standup is not ongoing but admin tries to perform action on it
	StandupNotOngoing = "Sorry Master, the Standup has not yet begun."
	// StandupFinished const is the response to the admin when standup has finished.
	StandupFinished = "Master, the standup has been completed."
	// StandupUserSkip const is the response to the user when user skips the standup
	StandupUserSkip = "Your standup for this week has been skipped."
	// StandupUserFinished const is the response to the user after they finish their standup
	StandupUserFinished = "Congratulations! You finished this week's standup! See you next week!"
	// StandupUserAlreadyFinished const is the response to the user if they have already finished standup
	StandupUserAlreadyFinished = "You have already finished this week's standup."
	// standupStartSkipMsg const is the suffix concatenation for users to start or skip standup.
	standupStartSkipMsg = "Type `start` to get started, or `skip` if you don't feel upto it this week."
	// StandupStartMessage const is the message sent to the users when standup is starting.
	StandupStartMessage = "Hope you are having a lovely day! It's time for Standup!\n" + standupStartSkipMsg
	// StandupNagMessage const is the message sent to the users when they haven't completed the standup yet
	StandupNagMessage = "Hmm. It looks like you still haven't completed the standup. It won't take but a few minutes.\n" + standupStartSkipMsg
	// SlashStandupHelp const is the response sent to admins on /standup help
	SlashStandupHelp = "`/standup help` - Print all the available commands\n" +
		"`/standup configure` - Start configuration of the standup\n" +
		"`/standup start` - Start the standup\n" +
		"`/standup stop` - Stop the standup\n" +
		"`/standup check` - Check the current status of the standup\n" +
		"`/standup schedule` - Schedule when the standup should start, and for how long\n" +
		"`/standup nag` - Nag the users that haven't completed the standup yet"
	// SlashStandupDefault const is the response sent to admins when unkown command is used followed by /standup
	SlashStandupDefault = "Sorry Master, that's not a registered command.\nPlease use `/standup help` to see the available options."
)
