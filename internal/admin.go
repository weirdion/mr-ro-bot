/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package internal to handle inner-workings of the bot
package internal

import (
	"log"
	"strings"

	"github.com/nlopes/slack"
)

// DoAdminsBidding function to handle when admin sends commands
func DoAdminsBidding(bot *Bot, message slack.Msg, botPrefix string) {
	messageText := strings.TrimPrefix(message.Text, botPrefix)
	channelID := message.Channel
	switch actualMessage := strings.ToLower(strings.TrimSpace(messageText)); actualMessage {
	case "refresh":
		SendRegularMessage(bot.RTM, AcceptAdminCommandResponse, channelID)
		log.Printf("[INFO] Refresh command received from admin")
		bot.Handler.ReadSlackBotAdmins(bot.RTM)
		// TODO: Do we want to show new admins and number of users?
	case "print-standup":
		bot.Handler.StandupHandler.PrintReport(bot.RTM, channelID)
	case "whoisadmin":
		SendRegularMessage(bot.RTM, AcceptAdminCommandResponse, channelID)
		// TODO
	case "whoisid":
		SendRegularMessage(bot.RTM, AcceptAdminCommandResponse, channelID)
		// TODO
	default:
		log.Printf("[WARN] Unkown admin command: %s\n", actualMessage)
		SendRegularMessage(bot.RTM, RejectAdminCommandResponse, channelID)
	}
}
