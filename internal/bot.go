/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package internal to handle inner-workings of the bot
package internal

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/nlopes/slack"
)

// Bot struct is a wrapper around the RTM instance and the MrRoHandler instance
type Bot struct {
	RTM         *slack.RTM
	Handler     *MrRoHandler
	connectChan chan bool
	BotDir      BotDir
	isModeDebug bool
}

// BotDir struct is a wrapper with all relevant directories for the bot
type BotDir struct {
	BotUserDir    string
	BotLogDir     string
	BotStandupDir string
}

// NewBot function to create a new instance of Bot struct
func NewBot(newRTM *slack.RTM, botDir BotDir, isModeDebug bool) *Bot {
	b := &Bot{
		RTM:         newRTM,
		Handler:     NewHandler(isModeDebug),
		connectChan: make(chan bool),
		BotDir:      botDir,
		isModeDebug: isModeDebug,
	}
	log.Println("[INFO] Bot created")
	return b
}

// Start function to do operations when bot is created
func (bot *Bot) Start(client *slack.Client, veriToken string) {
	bot.Handler.ReadSlackBotAdmins(bot.RTM) // pouplate handler when connected
	go bot.startSecureSlashServer(veriToken)
	isFirstRun := true
	for receivedValue := range bot.connectChan {
		log.Printf("[INFO] Received bot connect channel event %v", receivedValue)
		if receivedValue {
			if !isFirstRun {
				bot.RTM = client.NewRTM()
			}
			<-time.After(time.Second * 5)
			isFirstRun = false
			go bot.listenForRTMEvents()
		}
	}
}

// startSecureSlashServer function that starts and handles the incoming requests over TLS
func (bot *Bot) startSecureSlashServer(veriToken string) {
	var verificationToken string
	flag.StringVar(&verificationToken, "token", veriToken, "Mr.Ro Bot Verification Token")
	flag.Parse()
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
		fmt.Fprintf(w, "This is the slash server.")
	})
	mux.HandleFunc("/slack", func(w http.ResponseWriter, r *http.Request) {
		s, err := slack.SlashCommandParse(r)
		if err != nil {
			log.Printf("[WARN] Error happened parsing slash command %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Internal Server Error")
			return
		}

		if !s.ValidateToken(verificationToken) {
			log.Printf("[ERROR] Unauthorized token used")
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprintf(w, "Unauthorized token used.")
			return
		}

		switch s.Command {
		case "/standup":
			params := &slack.Msg{Text: s.Text}
			_, err := json.Marshal(params)
			if err != nil {
				log.Println("[ERROR] Standup: Error parsing command.")
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "Error parsing standup command.")
				return
			}
			log.Println("[INFO] Standup: Slash command triggered.")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprintf(w, "Slash command received")
			// Sleeping for half a second to make sure we don't hit timeout replying to slack api
			<-time.After(time.Second * 1)
			if bot.Handler.IsSlackBotAdmin(s.UserID) {
				bot.StandupParser(s)
			} else {
				SendRegularMessage(bot.RTM, RejectNonAdminResponse, s.ChannelID)
			}

		default:
			log.Println("[WARN] Unkown slash command used")
			fmt.Fprintf(w, "Unkown slash command.")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})
	cfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
	srv := &http.Server{
		Addr:         ":3000",
		Handler:      mux,
		TLSConfig:    cfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
	log.Println("[INFO] Slash command server starting...")
	go func() {
		log.Printf("[INDO] Sending request for starting RTM socket")
		bot.connectChan <- true
	}()
	err := srv.ListenAndServe()
	// err := srv.ListenAndServeTLS("server.crt", "server.key")
	if err != nil {
		log.Printf("Error occurred on the slash server %v", err)
		bot.connectChan <- false
	}
}

// listenForRTMEvents function to start listening for RTM events
func (bot *Bot) listenForRTMEvents() {
	log.Println("[INFO] Starting listening for RTM messages")
	go bot.RTM.ManageConnection()
	reconnect := true
Loop:
	for msg := range bot.RTM.IncomingEvents {
		switch event := msg.Data.(type) {
		case *slack.ConnectedEvent:
			log.Printf("[INFO] Connection counter: %d", event.ConnectionCount)

		case *slack.MessageEvent:
			bot.OnMessageReceived(event)

		case *slack.ReconnectUrlEvent:
			log.Printf("[INFO] ReconnectURL Event %v", event)

		case *slack.RTMError:
			log.Printf("[ERROR] RTMError: %s", event.Error())

		case *slack.ConnectionErrorEvent:
			log.Printf("[ERROR] ConnectionError: %s", event)

		case *slack.DisconnectedEvent:
			log.Printf("[WARN] Disconnected: (intentional? %v)", event.Intentional)

		case *slack.InvalidAuthEvent:
			log.Println("[ERROR] Invalid credentials")
			bot.RTM.Disconnect()
			reconnect = false
			break Loop

		default:
			// if bot.isModeDebug {
			// 	log.Printf("[DEBUG] Unhandled event received: %v", event)
			// }
		}
	}
	if reconnect {
		log.Printf("[INFO] Restarting RTM Server..")
		bot.connectChan <- true
	} else {
		log.Printf("[INFO] Error encountered, stopping server..")
		bot.connectChan <- false
	}
}
