/*
Copyright (C) 2018  Ankit Sadana

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package internal to handle inner-workings of the bot
package internal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"github.com/nlopes/slack"
)

// StandupParser function that handles all incoming slash commands
func (bot *Bot) StandupParser(slashParse slack.SlashCommand) {
	msg := &slack.Msg{
		Channel: slashParse.ChannelID,
		User:    slashParse.UserID,
		Text:    slashParse.Text}

	log.Printf("[INFO] Standup: Slash command %s received.", msg.Text)
	switch strings.ToLower(strings.TrimSpace(msg.Text)) {
	case "help":
		standupHelp(bot.RTM, msg.Channel)
	case "start":
		bot.standupStart(msg)
	case "stop":
		bot.standupStop(msg.Channel)
	case "check":
		bot.standupCheck(msg.Channel)
	case "configure":
		bot.RTM.Client.PostEphemeral(msg.Channel, msg.User)
	case "schedule":
		SendRegularMessage(bot.RTM, "Master, this command is currently under construction.", msg.Channel)
	case "nag":
		bot.standupNag(msg.Channel)
	default:
		SendRegularMessage(bot.RTM, SlashStandupDefault, msg.Channel)
	}
}

func standupHelp(rtm *slack.RTM, channelID string) {
	log.Println("[INFO] Admin requested help")
	SendRegularMessage(rtm, SlashStandupHelp, channelID)
}

func (bot *Bot) standupStart(message *slack.Msg) {
	if !bot.Handler.StandupHandler.IsStandupRunning {
		// Generate the path to config dir in current user home directory
		configFile := strings.Join([]string{bot.BotDir.BotUserDir, StandupConfigPath}, "/")
		if FileExists(configFile) {
			byteSlice, err2 := ioutil.ReadFile(configFile)
			if err2 != nil {
				SendRegularMessage(bot.RTM, StandupConfigFailed, message.Channel)
				log.Printf("[ERROR] Failed to read config file: %v", err2)
			} else {
				config := StandupConfig{}
				// Unmarshall JSON config file as struct StandupConfig
				err3 := json.Unmarshal(byteSlice, &config)
				if err3 != nil {
					SendRegularMessage(bot.RTM, StandupConfigFailed, message.Channel)
					log.Printf("[ERROR] Failed while parsing the config file: %v", err3)
				} else {
					// Starting standup
					bot.Handler.StandupHandler.PopulateIgnoredUsers(bot.Handler.SlackUsers, config)
					isStandupStarted, standupStartFailedReason := bot.Handler.StandupHandler.StartStandup(bot.RTM, config, message.Channel)
					if isStandupStarted {
						SendRegularMessage(bot.RTM, StandupStarted, message.Channel)
					} else {
						SendRegularMessage(bot.RTM, standupStartFailedReason, message.Channel)
					}
				}
			}
		} else {
			SendRegularMessage(bot.RTM, StandupConfigFailed, message.Channel)
		}
	} else {
		SendRegularMessage(bot.RTM, StandupAlreadyRunning, message.Channel)
	}
}

func (bot *Bot) standupStop(channelID string) {
	if bot.Handler.StandupHandler.IsStandupRunning {
		bot.Handler.StandupHandler.IsStandupRunning = false
		err := bot.Handler.StandupHandler.ExportReport()
		if err != nil {
			log.Printf("[WARN] Failed to upload standup report to admin: %v", err)
		} else {
			SendRegularMessage(bot.RTM, StandupFinished, channelID)
		}
	} else {
		SendRegularMessage(bot.RTM, StandupNotOngoing, channelID)
	}
}

func (bot *Bot) standupCheck(channelID string) {
	isStandupComplete, userStandupReport := bot.Handler.StandupHandler.CheckIfStandupIsComplete()
	if isStandupComplete {
		SendRegularMessage(bot.RTM, StandupFinished, channelID)
	} else {
		SendRegularMessage(bot.RTM, parseStandupCheckReport(userStandupReport), channelID)
	}
}

func (bot *Bot) standupNag(channelID string) {
	if bot.Handler.StandupHandler.IsStandupRunning {
		var stringBuilder strings.Builder
		stringBuilder.WriteString("Nagged Users:")
		for _, standupInstance := range bot.Handler.StandupHandler.StandupResponses {
			if !standupInstance.Finished {
				stringBuilder.WriteString(fmt.Sprintf("\n%s", standupInstance.Name))
				SendRegularMessage(bot.RTM, StandupNagMessage, standupInstance.ChannelID)
			}
		}
		SendRegularMessage(bot.RTM, stringBuilder.String(), channelID)
	} else {
		SendRegularMessage(bot.RTM, StandupNotOngoing, channelID)
	}
}
